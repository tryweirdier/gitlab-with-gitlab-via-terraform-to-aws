data "aws_ami" "GitLab-EE" {
  most_recent = true
  owners = ["782774275127"]
   
   filter {
# Grabs the most recent version of  GitLab to install. GitLab produces community AMIs. 
   name   = "name"
   values = ["GitLab EE*"]
# If you would prefer to manage which GitLab version is installed then comment out the line above & uncomment the line below.
  #  values = ["GitLab EE ${var.gitlab_version}"]
  }
}

resource "aws_instance" "GitLabEE" {
  #ami = "${data.aws_ami.GitLab-EE.id}"
  ami = data.aws_ami.GitLab-EE.id
  instance_type = var.GitLabEE_EC2_instance_type
  vpc_security_group_ids = [
    "${aws_security_group.HTTP_HTTPS_SSH.id}",
    "${aws_security_group.Allow_ICMP_GL.id}"
  ]
  key_name = var.ssh_key
  tags = {
    Name = "GitLab_EE-${random_string.zone.result}"
  }

}
