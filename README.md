# GitLab with Gitlab via Terraform on AWS

This project is a working example of how to deploy a base GitLab Enterprise Edition Open Core server into Amazon Web Services using Terraform Open Source.

A list of what is deployed:
* GitLab Enterprise Edition (version specified in Variables.tf) on AWS' EC2.
* A randomized host/subzone in AWS' Route53.
* Security groups to allow HTTP (TCP/80), HTTPS (TCP/443) & SSH (TCP/22) access on the EC2 instance from the world.
* Conneciton to an AWS S3 bucket to maintain Terraform state information.

## Getting Started

* Ensure the AWS security keys are valid.
* Update the CI/CD variables in the project.

### Prerequisites

* Working AWS security keys are needed in the CI/CD configuration.
* Terraform > 0.12. If you are using an older version of Terraform then you WILL receive an error.


### Installing

No installation is required. However, this can be run locally by following the Terraform commands:

``terraform init``

Initializes terraform; connects to S3 backend; download required modules

``terraform plan``

Verifies all the plans work.

``terraform apply``

Creates the infrastructure & GitLab instances

``terraform destroy ``

Destroys the infrastructure & instances that Terraform created.


## Running the tests

A testing stage is available in the GitLab CI/CD run using [terraform-compliance](https://github.com/eerkunt/terraform-compliance).


## Deployment

Deployment, as well as the destruction of infrastructure, is manual job in the pipeline.


## License

This project is licensed under the [MIT License](https://gitlab.com/khair1/gitlab-with-gitlab-via-terraform-on-aws/blob/master/LICENSE).

Other licenses include [Terraform's Mozilla Public License 2.0](https://github.com/hashicorp/terraform/blob/master/LICENSE) & [terraform-compliance's MIT license](https://github.com/eerkunt/terraform-compliance/blob/master/LICENSE)
